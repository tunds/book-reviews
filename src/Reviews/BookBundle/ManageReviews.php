<?php

namespace Reviews\BookBundle;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ManageReviews {

    protected $reviewsEntityManager, $tokenStorage, $container;

    public function __construct(EntityManager $entityManager, TokenStorage $tokenStorage, Container $container)
    {
        $this->reviewsEntityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->container = $container;

    }

    /**
     * Save a new review
     *
     * @param string $review The review
     * @param string $bookId The book id
     */
    public function saveReview($review, $bookId){

        $review->setUser( !is_string($this->tokenStorage->getToken()->getUser()) ? $this->tokenStorage->getToken()->getUser() : null);
        $review->setBooks($this->container->get('store.manage_books')->verifyBookExistsId($bookId));
        $review->setTimePosted(new \DateTime());

        $this->reviewsEntityManager->persist($review);
        $this->reviewsEntityManager->flush();
    }

    /**
     * Get the comments
     *
     * @param string $bookId The book's id
     * @return array The comments
     */
    public function getComments($bookId){
       return $this->reviewsEntityManager->getRepository('ReviewsBookBundle:Review')->findBy(array('books' => $this->container->get('store.manage_books')->verifyBookExistsId($bookId)->getId()), array('timePosted' => 'DESC'));
    }

}