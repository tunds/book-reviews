<?php
/**
 * Created by PhpStorm.
 * User: tundeadegoroye
 * Date: 02/04/2017
 * Time: 19:08
 */

namespace Reviews\BookBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBProvider;
use Symfony\Component\Security\Core\User\UserInterface;


class MyFOSUBUserProvider extends BaseFOSUBProvider
{
    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {

        // Get the username 
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        // Get token and ID
        $service = $response->getResourceOwner()->getName();
        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';

        if(null !== $existingUser = $this->userManager->findUserBy(array($property => $username))){

            // Clear user (disconnect user)
            $existingUser->$setter_id(null);
            $existingUser->$setter_token(null);
            $this->userManager->updateUser($existingUser);
        }

        // Update the current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());
        $this->userManager->updateUser($user);

    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $email = $response->getEmail();
        $name = str_replace(' ', '_', $response->getRealName());
        $user = $this->userManager->findUserByEmail($email);

        // if null just create new user and set it properties
        if (null === $user) {

            $service = $response->getResourceOwner()->getName();

            $setter = 'set'.ucfirst($service);
            $setter_id = $setter.'Id';
            $setter_token = $setter.'AccessToken';

            $user = $this->userManager->createUser();
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());

            $user->setUsername($name);
            $user->setEmail($email);
            $user->setEmailCanonical(strtolower($email));
            $user->setPassword($username);
            $user->setEnabled(true);
            $this->userManager->updateUser($user);

            return $user;
        }

        $user = parent::loadUserByOAuthUserResponse($response);
        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';

        //update access token
        $user->$setter($response->getAccessToken());
        return $user;
    }
}