<?php

namespace Reviews\BookBundle\Controller;

use Reviews\BookBundle\Form\BookType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{
    public function indexAction(Request $myRequest)
    {

        $form = $this->createForm(BookType::class, null, [
            'action' => $myRequest->getUri()
        ]);

        // If the request is post it'll populate the form
        $form->handleRequest($myRequest);

        if ($form->isSubmitted()){

            // Send the term to the search action
            $search_term = $myRequest->request->get('reviews_bookbundle_book')['title'];
            return $this->redirect($this->generateUrl('book_search',[
                'search_term' => $search_term
            ]));

        }

        return $this->render('ReviewsBookBundle:Page:index.html.twig', [
            'bookEntries' =>  $this->get('store.manage_books')->retrieveLatestBooks(8),
            'searchForm' => $form->createView()]);
    }

    public function searchAction(Request $myRequest)
    {

        $term = '';

        if ($myRequest->query->get('search_term')){

            $term = $myRequest->query->get('search_term');
        } else {
            $terms = $myRequest->request->get('reviews_bookbundle_book');

            if (!empty($terms['title'])){
                $term = $terms['title'];
            }
            if (!empty($terms['author'])){
                $term .= '+author:'.$terms['author'];
            }
            if (!empty($terms['isbn'])){
                $term .= '+isbn:'.$terms['isbn'];
            }
        }

        $form = $this->createForm(BookType::class, null, [
            'action' => $myRequest->getUri()
        ]);

        // If the request is post it'll populate the form
        $form->handleRequest($myRequest);

        if ($form->isSubmitted()){

            $term = $myRequest->request->get('reviews_bookbundle_book')['title'];
            return $this->render('@ReviewsBook/Page/search.html.twig', array(
                'term' => $term,
                'searchForm' => $form->createView(),
                'results' => $this->get('store.manage_books')->findGoogleApiBooks($term)
            ));
        }

        return $this->render('@ReviewsBook/Page/search.html.twig', array(
            'term' => $term,
            'searchForm' => $form->createView(),
            'results' => $this->get('store.manage_books')->findGoogleApiBooks($term)
        ));
    }


}
