<?php

namespace Reviews\BookBundle\Controller;

use Reviews\BookBundle\Entity\Review;
use Reviews\BookBundle\Form\ReviewType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Reviews\BookBundle\Entity\Book;
use Reviews\BookBundle\Form\BookType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReviewsController extends Controller
{
    public function viewBookReviewAction($id, Request $request)
    {

        // If the book doesn't exist
        if ($this->get('store.manage_books')->verifyBookExistsId($id) == null):
            throw new NotFoundHttpException("Page not found");
        endif;

        $reviewEntry = new Review();
        $form = $this->createForm(ReviewType::class, $reviewEntry, [
            'action' => $request->getUri()
        ]);

        $form->handleRequest($request);

        if ($form->isValid()){

            $this->get('store.reviews')->saveReview($reviewEntry, $id);

            return $this->redirect($this->generateUrl('view_book_review', [
                'id' => $this->get('store.manage_books')->verifyBookExistsId($id)->getId()
//                'book' => $this->get('store.manage_books')->verifyBookExistsId($id),
//                'form' => $form->createView(),
//                'comments' => $this->get('store.reviews')->getComments($id)
                ]));

        }

        return $this->render('ReviewsBookBundle:Reviews:view.html.twig', [
            'book' => $this->get('store.manage_books')->verifyBookExistsId($id),
            'comments' => $this->get('store.reviews')->getComments($id),
            'user' => $this->getUser(),
            'form' => $form->createView(),
            'bookExists' => $request->query->get('bookExists')]);
    }

    public function viewReviewedBooksAction(){

        return $this->render('ReviewsBookBundle:Page:reviews.html.twig', [
            'bookEntries' =>  $this->get('store.manage_books')->retrieveLatestBooks(5)]);
    }

    public function viewBookAction($id)
    {

        $trailer = '';
        $isbn = '';
        $book = $this->get('store.manage_books')->getGoogleApiBook($id, true);
        $film = $this->get('store.manage_books')->getRelatedFilm($book['volumeInfo']['title']);

        if (!empty($trailer)){
            $trailer = $this->get('store.manage_books')->getFilmTrailer($film[0]['id']);
        }

        if (!empty($book['volumeInfo']['industryIdentifiers'][0]['identifier'])){
            $isbn = $book['volumeInfo']['industryIdentifiers'][0]['identifier'];

            $book = $this->get('store.manage_books')->verifyBookExists($isbn);

            if(!empty($book)){

                return $this->redirect($this->generateUrl('view_book_review', [
                    'id' => $book->getId()
                ]));
            }
        }

        return $this->render('@ReviewsBook/Page/book.html.twig',[
            'book' => $book,
            'film' => $film,
            'trailerKey' => $trailer,
            'user' => $this->getUser()
        ]);
    }

    public function createBookAction($id, Request $request){

        // Get the book from the api
        $book = $this->get('store.manage_books')->getGoogleApiBook($id, true);
        $bookEntry = new Book();

        // Prepopulate the form with data
        if ($request->getMethod() === 'GET'){

            $bookEntry->setTitle($book['volumeInfo']['title']);
            $bookEntry->setIsbn($book['volumeInfo']['industryIdentifiers'][0]['identifier']);
            $bookEntry->setSummary(strip_tags($book['volumeInfo']['description']));
            $bookEntry->setReleaseDate(date_create_from_format('Y-m-d', $book['volumeInfo']['publishedDate']));
            $bookEntry->setAuthor($book['volumeInfo']['authors'][0]);

        }

        $form = $this->createForm(BookType::class, $bookEntry, [
            'action' => $request->getUri()
        ]);

        $form->handleRequest($request);

        if ($form->isValid()){

            // Get the image from the book object
            return $this->redirect($this->generateUrl('view_book_review', ['id' => $this->get('store.manage_books')->saveGoogleNewBook($bookEntry, $book)]));
        }

        return $this->render('@ReviewsBook/Reviews/create_new.html.twig',['form' => $form->createView(), 'bookCover' => $book['volumeInfo']['imageLinks']['small']]);
    }

    public function createBookReviewAction(Request $request)
    {

        $bookEntry = new Book();
        $form = $this->createForm(BookType::class, $bookEntry, [
            'action' => $request->getUri()
        ]);

        $form->handleRequest($request);

        if ($form->isValid()){

            $file = $request->files->get('reviews_bookbundle_book')['bookCover'];

            if ($file !== null) {

                $mimeType = $file->getClientMimeType();
                $size = $file->getClientSize();

                // Check the file type
                if (!in_array($mimeType, array("image/png", "image/jpeg", "image/jpg"))){

                    return $this->render('ReviewsBookBundle:Reviews:create.html.twig',['form' => $form->createView(), 'upload_error' => 'Image file not valid']);

                }

                // Check the file size
                if ($size > 60000){
                    return $this->render('ReviewsBookBundle:Reviews:create.html.twig',['form' => $form->createView(), 'upload_error' => 'Image file too large']);
                }

            } else {

                return $this->render('ReviewsBookBundle:Reviews:create.html.twig',['form' => $form->createView(), 'upload_error' => 'No image uploaded']);
            }

            // Verify the book doesn't already exist
            if ($this->get('store.manage_books')->verifyBookExists($form->get('isbn')->getData())):
                return $this->redirect($this->generateUrl('view_book_review', ['id' => $this->get('store.manage_books')->getBookID($form->get('isbn')->getData()), 'bookExists' => true]));
            endif;

            return $this->redirect($this->generateUrl('view_book_review', ['id' => $this->get('store.manage_books')->saveNewBook($bookEntry)]));

        }

        return $this->render('ReviewsBookBundle:Reviews:create.html.twig',['form' => $form->createView(), 'upload_error' => '']);
    }

    public function editBookReviewAction($id, Request $request)
    {

        // Make sure the user who created the book can edit it
        if (!$this->get('store.manage_books')->verifyUsersBook($id)):
            return $this->redirect($this->generateUrl('index'));
        endif;

        $bookEntry = $this->get('store.manage_books')->verifyBookExistsId($id);
        $cover = $bookEntry->getBookCover();
        $form = $this->createForm(BookType::class, $bookEntry, [
            'action' => $request->getUri()
        ]);

        $form->handleRequest($request);

        if ($form->isValid()):

            $bookEntry->setBookCover($cover);
            $id = $this->get('store.manage_books')->saveNewBook($bookEntry, false);
            return $this->redirect($this->generateUrl('view_book_review', ['id' => $id]));
        endif;

        return $this->render('ReviewsBookBundle:Reviews:edit.html.twig', [
            'form' => $form->createView(),
            'book' => $bookEntry
        ]);
    }

    public function deleteBookReviewAction($id)
    {
        $this->get('store.manage_books')->deleteBook($id);
        return $this->redirect($this->generateUrl('index'));
    }

}
