<?php

namespace Reviews\BookBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ReviewType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',null,array('label' => 'Review title','attr' => array('class' => 'form-control')))
                ->add('summary', null,array('label' => false,'attr' => array('class' => 'form-control')))
                ->add('rating', ChoiceType::class, array(
                        'choices' => array(
                            "Very Bad" => 1,
                            "Bad" => 2,
                            "Alright" => 3,
                            "Good" => 4,
                            "Very Good" => 5,
                        ),
                        'expanded' => true,
                        'multiple' => false,
                        'label' => 'Rating'
                    )
                )
                ->add('submit', SubmitType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Reviews\BookBundle\Entity\Review',
            'csrf_protection' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reviews_bookbundle_review';
    }


}
