<?php

namespace Reviews\BookBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class BookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',null,array('label' => 'Book title','attr' => array('class' => 'form-control')))
                ->add('isbn',null,array('label' => 'ISBN','attr' => array('class' => 'form-control')))
                ->add('summary',null,array('label' => 'Summary','attr' => array('class' => 'form-control')))
                ->add('releaseDate', DateType::class, array(
                    'years' => range(date('Y') + 0, date('Y') - 50),
                    'label' => 'Release date',
                    'attr' => array('class' => 'form-control')
                ), array('required' => 'false'))
                ->add('author',null,array('label' => 'Author','attr' => array('class' => 'form-control')))
                ->add('userReview',null,array('label' => 'Review','attr' => array('class' => 'form-control', 'rows' => 5)))
                ->add('rating', ChoiceType::class, array(
                        'choices' => array(
                            "Very Bad" => 1,
                            "Bad" => 2,
                            "Alright" => 3,
                            "Good" => 4,
                            "Very Good" => 5,
                        ),
                        'expanded' => true,
                        'multiple' => false,
                        'label' => 'Rating'
                    )
                )
                ->add('bookCover', FileType::class, array('data_class' => null, 'required' => false), array('label' => 'Book Cover Image'))
                ->add('submit', SubmitType::class, array('attr' => array('class' => 'btn btn-primary')));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Reviews\BookBundle\Entity\Book',
            'csrf_protection' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reviews_bookbundle_book';
    }


}
