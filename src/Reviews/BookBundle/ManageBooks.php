<?php

namespace Reviews\BookBundle;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;
use GuzzleHttp;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraints\DateTime;

class ManageBooks
{
    protected $bookEntityManager, $imageParam, $tokenStorage, $dbAPIKeyV3 = 'ecc3f618c21c329d1bae19339b66bd32';

    public function __construct(EntityManager $entityManager, $imageDirectoryParam, TokenStorage $tokenStorage){

        $this->bookEntityManager = $entityManager;
        $this->imageParam = $imageDirectoryParam;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Gets the latest books with a limit
     *
     * @param integer   $limit  The number of books to limit
     * @return \Doctrine\ORM\EntityRepository The Book repository class.
     */
    public function retrieveLatestBooks($limit) {

        return $this->bookEntityManager->getRepository('ReviewsBookBundle:Book')->findBy(array(), array('createdDate' => 'DESC'), $limit);

    }

    public function findGoogleApiBooks($term) {

        $client = new GuzzleHttp\Client();

        $res = $client->request('GET', 'https://www.googleapis.com/books/v1/volumes', [
            'query' => ['q' => $term,
                        'projection' => 'lite']
        ]);

        $data = json_decode($res->getBody(), true);

        return !empty($data["items"]) ? $data["items"] : null;

    }

    public function getGoogleApiBook($id, $detailed = false) {

        $client = new GuzzleHttp\Client();

        $params = $detailed ? [] : ['query' => ['projection' => 'lite']];

        $res = $client->request('GET', 'https://www.googleapis.com/books/v1/volumes/'.$id, $params);

        $data = json_decode($res->getBody(), true);

        return $data;

    }

    public function getRelatedFilm($title){

        $client = new GuzzleHttp\Client();

        $params = ['query' => ['api_key' => $this->dbAPIKeyV3,
                               'language' => 'en-US',
                                'query' => urlencode($title),
                                'page' => 1,
                                'include_adult' => false]];

        $res = $client->request('GET', 'https://api.themoviedb.org/3/search/movie', $params);

        $data = json_decode($res->getBody(), true);

        if (!empty($data['results'][0]['id'])) {

            $filmID = $data['results'][0]['id'];
            $data['results'][0]['trailer_key'] = $this->getFilmTrailer($filmID);
        }

        return $data['results'];

    }

    public function getFilmTrailer($id){

        $client = new GuzzleHttp\Client();

        $params = ['query' => ['api_key' => $this->dbAPIKeyV3]];

        $res = $client->request('GET', 'https://api.themoviedb.org/3/movie/'. $id .'/videos', $params);

        $data = json_decode($res->getBody(), true);

        return $data['results'][0]['key'];

    }

    /**
     * Gets the books with a search term
     *
     * @param string $term  The search term
     * @return array The books
     */
    public function findBooks($term) {

        $bookEntity = $this->bookEntityManager->getRepository('ReviewsBookBundle:Book');

        $query = $bookEntity->createQueryBuilder('b')
            ->where('b.title LIKE :title')
            ->orWhere('b.isbn = :isbn')
            ->setParameter('title', '%'.$term.'%')
            ->setParameter('isbn',$term)
            ->orderBy('b.createdDate','DESC')
            ->getQuery();

        return $query->getResult();

    }

    /**
     * Checks to see if the book already exists with the id
     *
     * @param string $isbn The book isbn
     * @return array The book
     */
    public function verifyBookExists($isbn){

        return $this->bookEntityManager->getRepository('ReviewsBookBundle:Book')->findOneBy(array("isbn" => $isbn));

    }

    /**
     * Checks to see if the book already exists with the id
     *
     * @param integer $id The book id
     * @return array The book
     */
    public function verifyBookExistsId($id){
        return $this->bookEntityManager->getRepository('ReviewsBookBundle:Book')->find($id);
    }

    /**
     * Checks to see if the book belongs to the user
     *
     * @param integer $id The book id
     * @return boolean The result
     */
    public function verifyUsersBook($id){

        if($this->bookEntityManager->getRepository('ReviewsBookBundle:Book')->find($id)->getUser() === $this->tokenStorage->getToken()->getUser()){
            return true;
        }
        return false;
    }


    /**
     * Get the book's id
     *
     * @param string $isbn The book isbn
     * @return array The book
     */
    public function getBookID($isbn){

        return $this->bookEntityManager->getRepository('ReviewsBookBundle:Book')->findOneBy(array("isbn" => $isbn))->getId();

    }

    /**
     * Delete the book
     *
     * @param string $id The book id
     */
    public function deleteBook($id){

        $this->bookEntityManager->remove($this->bookEntityManager->getRepository('ReviewsBookBundle:Book')->find($id));
        $this->bookEntityManager->flush();

    }

    /**
     * Save a new Google book
     *
     * @param string $book The book
     * @return integer The book id
     */
    public function saveGoogleNewBook($book, $googleBook){

        $book->setGoogleBookId($googleBook['id']);
        $book->setReleaseDate(date_create_from_format('Y-m-d', $googleBook['volumeInfo']['publishedDate']));
        $book->setCreatedDate(new DateTime());
        $book->setUser($this->tokenStorage->getToken()->getUser());
        $book->setCreatedDate(new \DateTime());

        // Get the file from the url, size & type
        // Finally save the image to the directory
        $filename = md5(uniqid()) . '.' . 'jpg';
        $bookDirectory = $this->imageParam;
        file_put_contents($bookDirectory . "/". $filename, @file_get_contents($googleBook['volumeInfo']['imageLinks']['medium']));
        $book->setBookCover($filename);

        $this->bookEntityManager->persist($book);
        $this->bookEntityManager->flush();

        return $book->getId();
    }

    /**
     * Save a new book
     *
     * @param string $book The book
     * @return integer The book id
     */
    public function saveNewBook($book, $updateCover = true){

        if ($updateCover) {
            $book->setBookCover($this->saveImage($book));
        }

        $book->setUser($this->tokenStorage->getToken()->getUser());
        $book->setCreatedDate(new \DateTime());

        $this->bookEntityManager->persist($book);
        $this->bookEntityManager->flush();

        return $book->getId();
    }

    /**
     * Save a new image
     *
     * @param string $book The book
     * @return string The new filename
     */
    private function saveImage($book){

        /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
        $file = $book->getBookCover();
        $fileName = md5(uniqid() .'.'.$file->guessExtension());

        $file->move(
            $this->imageParam,
            $fileName
        );

        return $fileName;

    }
}
