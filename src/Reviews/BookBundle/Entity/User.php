<?php

namespace Reviews\BookBundle\Entity;

use FOS\UserBundle\Model\User as FOSUBUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * User
 *
 * @ORM\Table(name="sta286_user")
 * @ORM\Entity(repositoryClass="Reviews\BookBundle\Repository\UserRepository")
 */
class User extends FOSUBUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     */
    private $facebookAccessToken;

    /**
     * @Serializer\Exclude()
     * @ORM\OneToMany(targetEntity="Reviews\BookBundle\Entity\Book", mappedBy="user")
     */
    private $books;

    /**
     * @Serializer\Exclude()
     * @ORM\OneToMany(targetEntity="Reviews\BookBundle\Entity\Review", mappedBy="user")
     *
     */

    private $reviews;

    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Add book
     *
     * @param \Reviews\BookBundle\Entity\Book $book
     *
     * @return User
     */
    public function addBook(\Reviews\BookBundle\Entity\Book $book)
    {
        $this->books[] = $book;

        return $this;
    }

    /**
     * Remove book
     *
     * @param \Reviews\BookBundle\Entity\Book $book
     */
    public function removeBook(\Reviews\BookBundle\Entity\Book $book)
    {
        $this->books->removeElement($book);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * Add review
     *
     * @param \Reviews\BookBundle\Entity\Review $review
     *
     * @return User
     */
    public function addReview(\Reviews\BookBundle\Entity\Review $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \Reviews\BookBundle\Entity\Review $review
     */
    public function removeReview(\Reviews\BookBundle\Entity\Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     *
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }
}
