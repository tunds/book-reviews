<?php

namespace Reviews\BookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Book
 *
 * @ORM\Table(name="sta286_book")
 * @ORM\Entity(repositoryClass="Reviews\BookBundle\Repository\BookRepository")
 */
class Book
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="google_book_id", type="string", length=255, nullable=true)
     */
    private $googleBookId;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="isbn", type="string", length=255)
     */
    private $isbn;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="summary", type="text")
     */
    private $summary;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="user_review", type="text")
     */
    private $userReview;

    /**
     * @var \DateTime
     * @Assert\NotBlank()
     * @ORM\Column(name="release_date", type="date")
     */
    private $releaseDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime")
     */
    private $createdDate;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="author", type="string", length=255)
     */
    private $author;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="rating", type="integer")
     */
    private $rating;

    /**
     * @ORM\Column(name="book_cover", type="string", nullable=true)
     */
    private $bookCover;

    /**
     * @ORM\ManyToOne(targetEntity="Reviews\BookBundle\Entity\User", inversedBy="books")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */

    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Reviews\BookBundle\Entity\Review", mappedBy="books")
     */
    private $reviews;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set isbn
     *
     * @param string $isbn
     *
     * @return Book
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * Get isbn
     *
     * @return string
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return Book
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set releaseDate
     *
     * @param \DateTime $releaseDate
     *
     * @return Book
     */
    public function setReleaseDate($releaseDate)
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * Get releaseDate
     *
     * @return \DateTime
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Book
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reviews = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set user
     *
     * @param \Reviews\BookBundle\Entity\User $user
     *
     * @return Book
     */
    public function setUser(\Reviews\BookBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Reviews\BookBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add review
     *
     * @param \Reviews\BookBundle\Entity\Review $review
     *
     * @return Book
     */
    public function addReview(\Reviews\BookBundle\Entity\Review $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param \Reviews\BookBundle\Entity\Review $review
     */
    public function removeReview(\Reviews\BookBundle\Entity\Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Get reviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return Book
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set userReview
     *
     * @param string $userReview
     *
     * @return Book
     */
    public function setUserReview($userReview)
    {
        $this->userReview = $userReview;

        return $this;
    }

    /**
     * Get userReview
     *
     * @return string
     */
    public function getUserReview()
    {
        return $this->userReview;
    }

    /**
     * Set bookCover
     *
     * @param string $bookCover
     *
     * @return Book
     */
    public function setBookCover($bookCover)
    {
        $this->bookCover = $bookCover;

        return $this;
    }

    /**
     * Get bookCover
     *
     * @return string
     */
    public function getBookCover()
    {
        return $this->bookCover;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Book
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set googleBookId
     *
     * @param string $googleBookId
     *
     * @return Book
     */
    public function setGoogleBookId($googleBookId)
    {
        $this->googleBookId = $googleBookId;

        return $this;
    }

    /**
     * Get googleBookId
     *
     * @return string
     */
    public function getGoogleBookId()
    {
        return $this->googleBookId;
    }
}
