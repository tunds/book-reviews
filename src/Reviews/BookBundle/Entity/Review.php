<?php

namespace Reviews\BookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Review
 *
 * @ORM\Table(name="sta286_review")
 * @ORM\Entity(repositoryClass="Reviews\BookBundle\Repository\ReviewRepository")
 */
class Review
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="summary", type="text")
     */
    private $summary;

    /**
     * @var string
     *
     * @ORM\Column(name="time_posted", type="datetime")
     */

    private $timePosted;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="rating", type="integer")
     */
    private $rating;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=50)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="Reviews\BookBundle\Entity\User", inversedBy="reviews")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */

    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Reviews\BookBundle\Entity\Book", inversedBy="reviews")
     * @ORM\JoinColumn(name="book_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $books;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return Review
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Review
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set user
     *
     * @param \Reviews\BookBundle\Entity\User $user
     *
     * @return Review
     */
    public function setUser(\Reviews\BookBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Reviews\BookBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set timePosted
     *
     * @param \DateTime $timePosted
     *
     * @return Review
     */
    public function setTimePosted($timePosted)
    {
        $this->timePosted = $timePosted;

        return $this;
    }

    /**
     * Get timePosted
     *
     * @return \DateTime
     */
    public function getTimePosted()
    {
        return $this->timePosted;
    }

    /**
     * Set books
     *
     * @param \Reviews\BookBundle\Entity\Book $books
     *
     * @return Review
     */
    public function setBooks(\Reviews\BookBundle\Entity\Book $books)
    {
        $this->books = $books;

        return $this;
    }

    /**
     * Get books
     *
     * @return \Reviews\BookBundle\Entity\Book
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Review
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }
}
