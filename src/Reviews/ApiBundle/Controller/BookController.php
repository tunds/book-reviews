<?php

namespace Reviews\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use JMS\Serializer\SerializationContext;
use Reviews\ApiBundle\Serializer\Exclusion\FieldsListExclusionStrategy;
use Reviews\BookBundle\Entity\Book;
use Reviews\BookBundle\Entity\Review;
use Reviews\BookBundle\Form\BookType;
use Reviews\BookBundle\Form\ReviewType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class BookController extends FOSRestController {

    /**
     * MARK: Functions
     */

    private function includeValues($entries, $fields) {

        // Get the serializer service
        $serializer = $this->get('serializer');

        // Create a context, get the fields you want to include and add the exclusion strategy
        $context = new SerializationContext();
        $fieldList = $fields;
        $context->addExclusionStrategy(
            new FieldsListExclusionStrategy($fieldList)
        );

        return $serializer->serialize($entries, 'json', $context);

    }

    /**
     * MARK: GET ACTIONS
     */

    /**
     * @QueryParam(name="sort", nullable=true, requirements="(newest|oldest)", default="newest")
     * @QueryParam(name="page", nullable=true, requirements="\d+", default="1")
     * @QueryParam(name="limit", nullable=true, requirements="\d+", default="3")
     */
    // Get all of the books
    public function getBooksAction(ParamFetcher $paramFetcher)
    {

        $entries = null;
        $order = $paramFetcher->get('sort');
        $offset = ((int)$paramFetcher->get('page') - 1) * (int)$paramFetcher->get('limit');
        $em = $this->getDoctrine()->getManager();

        if ($order === 'newest'){
            $entries = $em->getRepository('ReviewsBookBundle:Book')->findBy(array(), array('createdDate' => 'DESC'), (int)$paramFetcher->get('limit'), $offset);
        } else {
            $entries = $em->getRepository('ReviewsBookBundle:Book')->findBy(array(), array('createdDate' => 'ASC'), (int)$paramFetcher->get('limit'), $offset);
        }

        $results = $this->includeValues($entries, ['id','title','summary','bookCover','user','username']);
        return $this->handleView($this->view(json_decode($results, true)));

    }

    // Get a single book
    public function getBookAction($id){

        $em = $this->getDoctrine()->getManager();
        $entry = $em->getRepository('ReviewsBookBundle:Book')->find($id);
        $entry ? $view = $this->view($entry) : $view = $this->view(null, 404);
        return $this->handleView($view);

    }

    /**
     * @QueryParam(name="sort", nullable=true, requirements="(newest|oldest)", default="newest")]
     * @QueryParam(name="page", nullable=true, requirements="\d+", default="1")
     * @QueryParam(name="limit", nullable=true, requirements="\d+", default="3")
     */
    // Get all of the comments
    public function getCommentsAction(ParamFetcher $paramFetcher)
    {
        $entries = null;
        $order = $paramFetcher->get('sort');
        $offset = ((int)$paramFetcher->get('page') - 1) * (int)$paramFetcher->get('limit');
        $em = $this->getDoctrine()->getManager();

        if ($order === 'newest'){
            $entries = $em->getRepository('ReviewsBookBundle:Review')->findBy(array(), array('timePosted' => 'DESC'), (int)$paramFetcher->get('limit'), $offset);
        } else {
            $entries = $em->getRepository('ReviewsBookBundle:Review')->findBy(array(), array('timePosted' => 'ASC'), (int)$paramFetcher->get('limit'), $offset);
        }

        $results = $this->includeValues($entries, ['id','title','summary','timePosted','user','username']);
        return $this->handleView($this->view(json_decode($results, true)));

    }

    // Get a single comment
    public function getCommentAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entry = $em->getRepository('ReviewsBookBundle:Review')->find($id);
        $results = $this->includeValues($entry, ['id','title','summary','timePosted','user','username']);
        $entry ? $view = $this->view(json_decode($results, true)) : $view = $this->view(null, 404);
        return $this->handleView($view);
    }

    /**
     * @QueryParam(name="page", nullable=true, requirements="\d+", default="1")
     * @QueryParam(name="limit", nullable=true, requirements="\d+", default="5")
     */
    // Get all of the books
    public function getUsersAction(ParamFetcher $paramFetcher)
    {

        $offset = ((int)$paramFetcher->get('page') - 1) * (int)$paramFetcher->get('limit');
        $em = $this->getDoctrine()->getManager();
        $entries = $em->getRepository('ReviewsBookBundle:User')->findBy(array(), null, (int)$paramFetcher->get('limit'), $offset);
        return $this->handleView($this->view($entries));

    }

    public function getUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entry = $em->getRepository('ReviewsBookBundle:User')->find($id);
        $entry ? $view = $this->view($entry) : $view = $this->view(null, 404);
        return $this->handleView($view);
    }

    /**
     * MARK: POST ACTIONS
     */

    // Create a new book
    public function postBookAction(Request $request){

        $bookEntry = new Book();
        $form = $this->createForm(BookType::class, $bookEntry);

        if($request->getContentType() != 'json') {
            return $this->handleView($this->view(null, 400));
        }

        $form->submit(json_decode($request->getContent(), true));

        if($form->isValid()) {

            // Get the file from the url, size & type
            $file = @file_get_contents($bookEntry->getBookCover());
            $type = pathinfo($bookEntry->getBookCover());
            $size = strlen(@file_get_contents($bookEntry->getBookCover()));

            if ($file){

                if (in_array($type['extension'], array("png", "jpeg", "jpg"))) {

                    // Check the size of the file is larger than 60kb
                    if ($size < 60000){

                        // Finally save the image to the directory
                        $filename = md5(uniqid()) . '.' . $type['extension'];
                        file_put_contents($this->getParameter('book_directory') . "/". $filename, $file);

                        // Replace the file image with new filename and save the object
                        $em = $this->getDoctrine()->getManager();
                        $bookEntry->setBookCover($filename);
                        $bookEntry->setUser($this->getUser());
                        $bookEntry->setCreatedDate(new \DateTime());

                        $em->persist($bookEntry);
                        $em->flush();

                        return $this->handleView($this->view(null, 201)->setLocation($this->generateUrl('api_review_get_books', ['id' => $bookEntry->getId()])));

                    } else {

                        // Send back a message and the appropriate status code
                        return $this->handleView($this->view("The image included in the request is too large, please ensure it's less than 60kb.", 400));
                    }

                } else {

                    // Send back a message and the appropriate status code
                    return $this->handleView($this->view("Request doesn't include a valid image format, please ensure it's png or jpg.", 400));
                }

            } else {

                // Send back a message and the appropriate status code
                return $this->handleView($this->view("Resource couldn't be found.", 404));
            }

        } else {

            return $this->handleView($this->view($form, 400));
        }

    }

    /**
     * MARK: PUT ACTIONS
     */

    // Update a book
    public function putBookAction($id, Request $request){

        $em = $this->getDoctrine()->getManager();
        $bookEntry = $em->getRepository('ReviewsBookBundle:Book')->find($id);

        $form = $this->createForm(BookType::class, $bookEntry);

        // Make sure the object is json
        if($request->getContentType() != 'json') {
            return $this->handleView($this->view(null, 400));
        }

        // json_decode the request content and pass it to the form
        $form->submit(json_decode($request->getContent(), true));

        // Validate the form
        if($form->isValid()) {

            // Get the file from the url, size & type
            $file = @file_get_contents($bookEntry->getBookCover());
            $type = pathinfo($bookEntry->getBookCover());
            $size = strlen(@file_get_contents($bookEntry->getBookCover()));
            if ($file){


                if (in_array($type['extension'], array("png", "jpeg", "jpg"))) {

                    // Check the size of the file is larger than 60kb
                    if ($size < 60000){

                        // Finally save the image to the directory
                        $filename = md5(uniqid()) . '.' . $type['extension'];
                        file_put_contents($this->getParameter('book_directory') . "/". $filename, $file);

                        // Replace the file image with new filename and save the object
                        $em = $this->getDoctrine()->getManager();
                        $bookEntry->setBookCover($filename);

                        $em->flush();

                        return $this->handleView($this->view(null, 204)->setLocation($this->generateUrl('api_review_get_books', ['id' => $bookEntry->getId()])));

                    } else {

                        // Send back a message and the appropriate status code
                        return $this->handleView($this->view("The image included in the request is too large, please ensure it's less than 60kb.", 400));
                    }

                } else {

                    // Send back a message and the appropriate status code
                    return $this->handleView($this->view("Request doesn't include a valid image format, please ensure it's png or jpg.", 400));
                }

            } else {

                // Send back a message and the appropriate status code
                return $this->handleView($this->view("Resource couldn't be found.", 404));
            }

        } else {
            return $this->handleView($this->view($form, 400));
        }

    }

    // Update a comment
    public function putCommentAction($id, Request $request){

        $em = $this->getDoctrine()->getManager();
        $reviewEntry = $em->getRepository('ReviewsBookBundle:Review')->find($id);

        $form = $this->createForm(ReviewType::class, $reviewEntry);

        // Make sure the object is json
        if($request->getContentType() != 'json') {
            return $this->handleView($this->view(null, 400));
        }

        // json_decode the request content and pass it to the form
        $form->submit(json_decode($request->getContent(), true));

        // Validate the form
        if($form->isValid()) {

            // Replace the file image with new filename and save the object
            $em = $this->getDoctrine()->getManager();

            $em->flush();
            return $this->handleView($this->view(null, 204)->setLocation($this->generateUrl('api_review_get_books', ['id' => $reviewEntry->getBooks()->getId()])));
        } else {
            return $this->handleView($this->view($form, 400));
        }

    }

    /**
     * MARK: DELETE ACTIONS
     */

    public function deleteBookAction($id){

        $em = $this->getDoctrine()->getManager();
        $entry = $em->getRepository('ReviewsBookBundle:Book')->find($id);

        if (!$entry){
            return $this->handleView($this->view(null, 404)->setLocation($this->generateUrl('index')));
        } else {

            $em->remove($entry);
            $em->flush();
            return $this->handleView($this->view(null, 204)->setLocation($this->generateUrl('index')));
        }
    }

    public function deleteCommentAction($id){

        $em = $this->getDoctrine()->getManager();
        $entry = $em->getRepository('ReviewsBookBundle:Review')->find($id);

        if (!$entry){
            return $this->handleView($this->view(null, 404)->setLocation($this->generateUrl('index')));
        } else {

            $em->remove($entry);
            $em->flush();
            return $this->handleView($this->view(null, 204)->setLocation($this->generateUrl('index')));
        }
    }

    /**
     * MARK: GET ACTIONS (Subresources)
     */

    /**
     * @QueryParam(name="sort", nullable=true, requirements="(newest|oldest)", default="newest")
     * @QueryParam(name="page", nullable=true, requirements="\d+", default="1")
     * @QueryParam(name="limit", nullable=true, requirements="\d+", default="3")
     */
    // Get all of the comments related to a book
    public function getBookCommentsAction(ParamFetcher $paramFetcher, $id){

        $entry = null;
        $order = $paramFetcher->get('sort');
        $offset = ((int)$paramFetcher->get('page') - 1) * (int)$paramFetcher->get('limit');
        $em = $this->getDoctrine()->getManager();

        if ($order === 'newest'){
            $entry = $em->getRepository('ReviewsBookBundle:Review')->findBy(array("books" => $id), array('timePosted' => 'DESC'), (int)$paramFetcher->get('limit'), $offset);
        } else {
            $entry = $em->getRepository('ReviewsBookBundle:Review')->findBy(array("books" => $id), array('timePosted' => 'ASC'), (int)$paramFetcher->get('limit'), $offset);
        }

        $results = $this->includeValues($entry, ['id','title','summary','timePosted','user','username']);
        $entry ? $view = $this->view(json_decode($results, true)) : $view = $this->view(null, 404);
        return $this->handleView($view);
    }

    /**
     * @QueryParam(name="sort", nullable=true, requirements="(newest|oldest)", default="newest")
     * @QueryParam(name="page", nullable=true, requirements="\d+", default="1")
     * @QueryParam(name="limit", nullable=true, requirements="\d+", default="3")
     */
    // Get all of the comments related to a user
    public function getUserCommentsAction(ParamFetcher $paramFetcher, $id){

        $entry = null;
        $order = $paramFetcher->get('sort');
        $offset = ((int)$paramFetcher->get('page') - 1) * (int)$paramFetcher->get('limit');
        $em = $this->getDoctrine()->getManager();

        if ($order === 'newest'){
            $entry = $em->getRepository('ReviewsBookBundle:Review')->findBy(array("user" => $id), array('timePosted' => 'DESC'), (int)$paramFetcher->get('limit'), $offset);
        } else {
            $entry = $em->getRepository('ReviewsBookBundle:Review')->findBy(array("user" => $id), array('timePosted' => 'ASC'), (int)$paramFetcher->get('limit'), $offset);
        }

        $results = $this->includeValues($entry, ['id','title','summary','timePosted']);
        $entry ? $view = $this->view(json_decode($results, true)) : $view = $this->view(null, 404);
        return $this->handleView($view);
    }

    /**
     * @QueryParam(name="sort", nullable=true, requirements="(newest|oldest)", default="newest")
     * @QueryParam(name="page", nullable=true, requirements="\d+", default="1")
     * @QueryParam(name="limit", nullable=true, requirements="\d+", default="3")
     */
    // Get all of the comments related to a user
    public function getUserBooksAction(ParamFetcher $paramFetcher, $id){

        $entry = null;
        $order = $paramFetcher->get('sort');
        $offset = ((int)$paramFetcher->get('page') - 1) * (int)$paramFetcher->get('limit');
        $em = $this->getDoctrine()->getManager();

        if ($order === 'newest'){
            $entry = $em->getRepository('ReviewsBookBundle:Book')->findBy(array("user" => $id), array('createdDate' => 'DESC'), (int)$paramFetcher->get('limit'), $offset);
        } else {
            $entry = $em->getRepository('ReviewsBookBundle:Book')->findBy(array("user" => $id), array('createdDate' => 'ASC'), (int)$paramFetcher->get('limit'), $offset);
        }

        $results = $this->includeValues($entry, ['id','title','summary','createdDate']);
        $entry ? $view = $this->view(json_decode($results, true)) : $view = $this->view("No books found", 404);
        return $this->handleView($view);
    }

    /**
     * MARK: POST ACTIONS (Subresources)
     */

    // Create a new comment
    public function postBookCommentAction(Request $request, $id){

        $em = $this->getDoctrine()->getManager();
        $entry = $em->getRepository('ReviewsBookBundle:Book')->find($id);

        if(!$entry){
            return $this->handleView($this->view("Book couldn't be found.", 404));
        }

        $reviewEntry = new Review();
        $form = $this->createForm(ReviewType::class, $reviewEntry);

        if($request->getContentType() != 'json') {
            return $this->handleView($this->view(null, 400));
        }

        $form->submit(json_decode($request->getContent(), true));

        // Validate the form
        if($form->isValid()) {

            // Replace the file image with new filename and save the object
            $em = $this->getDoctrine()->getManager();
            $reviewEntry->setUser($this->getUser());
            $reviewEntry->setTimePosted(new \DateTime());
            $reviewEntry->setBooks($entry);

            $em->persist($reviewEntry);
            $em->flush();

            return $this->handleView($this->view(null, 201)->setLocation($this->generateUrl('api_review_get_books', ['id' => $entry->getId()])));

        } else {

            return $this->handleView($this->view($form, 400));

        }
    }

}
