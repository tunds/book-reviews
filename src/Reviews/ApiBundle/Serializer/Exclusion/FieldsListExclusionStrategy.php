<?php

namespace Reviews\ApiBundle\Serializer\Exclusion;

use JMS\Serializer\Exclusion\ExclusionStrategyInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\Metadata\PropertyMetadata;
use JMS\Serializer\Context;

class FieldsListExclusionStrategy implements ExclusionStrategyInterface
{

    /**
     * Help from https://jolicode.com/blog/how-to-implement-your-own-fields-inclusion-rules-with-jms-serializer
     */

    private $fields = array();

    public function __construct(array $fields)
    {
        $this->fields = $fields;
    }

    /**
     * Whether the class should be skipped.
     *
     * @param ClassMetadata $metadata
     *
     * @return boolean
     */
    public function shouldSkipClass(ClassMetadata $metadata, Context $context)
    {
        return false;
    }

    /**
     * Whether the property should be skipped.
     *
     * @param PropertyMetadata $property
     *
     * @return boolean
     */
    public function shouldSkipProperty(PropertyMetadata $property, Context $context)
    {
        if (empty($this->fields)) {
            return false;
        }

        $name = $property->serializedName ? : $property->name;

        return !in_array($name, $this->fields);
    }
}